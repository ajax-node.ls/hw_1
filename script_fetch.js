const divList = document.querySelector('.list');

let filmRequest = fetch('https://swapi.co/api/films/');


filmRequest
    .then(response =>
        response.json()
    )
    .then(res => {
        let films = res.results;
        console.log(films);
        films.forEach((el) => {
            divList.appendChild(showListOfFilms(el));
        })
    })
.catch(function(err) {
    console.log('Fetch Error :-S', err);
});


let showListOfFilms = function (el) {
    const container = document.createElement('div');
    let charactersBox = document.createElement("div");
    let arrCharacters = [];
    let inner = `
    <h4>TITLE: ${el.title}</h4>
    <p>EPISOD_ID: ${el.episode_id}</p>
    <p>SUMMARY: ${el.opening_crawl}</p>
     `;
    charactersBox.innerHTML = '<p>CHARACTERS OF THE SERIES</p>';
    el.characters.forEach(item =>{
        showCharacters(item, arrCharacters, charactersBox);
    });
    container.innerHTML = inner;
    container.appendChild(charactersBox);
    return container;
};


let showCharacters = function (item, array, charactersBox) {
    let newRequest = fetch(`${item}`);
    newRequest.then(response => response.json())
        .then(res => {
            let character = res.name;
            console.log(character);
            array.push(`<p>${character}</p>`);
            charactersBox.innerHTML = `<h5>CHARACTERS OF THE SERIES:</h5> ${array.join('')}`;
        })
   };

