const divList = document.querySelector('.list');
let filmRequest = new XMLHttpRequest();
filmRequest.open('GET', 'https://swapi.co/api/films/', true);

filmRequest.onreadystatechange = () => {
    if (filmRequest.readyState == 4) {
        let response = JSON.parse(filmRequest.response);
        let films = response.results;
        let characters = films.map((item) => {
            return item.characters;
        });

        console.log(films);
        console.log(characters);

        if (films && characters) {
            films.forEach((el) => {
                divList.appendChild(showListOfFilms(el));
            });
        }

    }
};


filmRequest.send();


function showListOfFilms(el) {
    const container = document.createElement('div');
    let charactersBox = document.createElement("div");
    let arrCharacters = [];
    let inner = `
    <h4>TITLE: ${el.title}</h4>
    <p>EPISOD_ID: ${el.episode_id}</p>
    <p>SUMMARY: ${el.opening_crawl}</p> 
     `;
    charactersBox.innerHTML = '<p>CHARACTERS OF THE SERIES</p>';
    el.characters.forEach(item =>{
       showCharacters(item, arrCharacters, charactersBox);
    });
    container.innerHTML = inner;
    container.appendChild(charactersBox);
    return container;
}

function showCharacters(item, array, charactersBox) {

    let newRequest = new XMLHttpRequest();
        newRequest.open("GET", `${item}`, true);
        newRequest.send();
        newRequest.onreadystatechange = () => {
            if (newRequest.readyState == 4) {
                let response = JSON.parse(newRequest.response);
                array.push(`<p>${response.name}</p>`);
                charactersBox.innerHTML = `<h5>CHARACTERS OF THE SERIES:</h5> ${array.join('')}`;
            }

        };

}


